//
//  UISoundImageView.swift
//  Relax Melodies
//
//  Created by Alessandro Palermo on 09/12/2019.
//

import Foundation
import UIKit

var coder: NSCoder!

class UISoundImageView : UIImageView {
    var category : String = ""
    var sound: Sound = Sound(name: "default")
    var controllerToNotify: ViewController!
    var rotation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")

    
    init?(controller: ViewController) {
        self.controllerToNotify = controller

        super.init(coder: coder)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
  
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        // Tap to play sound
        if !self.sound.isPlaying{
                   UIView.animate(withDuration: 0.5, delay: 0, options: [],
                                  animations: {
                                   self.controllerToNotify.volumeView.alpha = 1
                                    self.controllerToNotify.volumeLabel.text = (self.restorationIdentifier)!
                       }, completion: nil)

            animateView()


            self.sound.playSound(name: (self.restorationIdentifier?.lowercased())!+"Sound")
            self.controllerToNotify.playMixMusic()
            
            self.controllerToNotify.playingSounds+=1
            if self.controllerToNotify.playingSounds==1{
                self.controllerToNotify.mixMusicLabel.text = (self.restorationIdentifier)!
            } else if self.controllerToNotify.playingSounds==2 {
                self.controllerToNotify.mixMusicLabel.text! += " & " + (self.restorationIdentifier)!
            } else {
                self.controllerToNotify.mixMusicLabel.text = String(self.controllerToNotify.playingSounds) + " contents"
            }

            UIView.animate(withDuration: 0.5, delay: 0, options: [],
                                            animations: {
                                                self.controllerToNotify.mixMusicView.alpha=1
            }, completion: nil)
            self.controllerToNotify.playPauseButton.isSelected = true
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                UIView.animate(withDuration: 0.5, delay: 0, options: [],
                                                animations: {
                                                 self.controllerToNotify.volumeView.alpha = 0
                                     }, completion: nil)
            }
               }
               
         // Tap to stop sound
            else{
           
                stopAnimateView()
                self.sound.stopSound()
                self.controllerToNotify.playingSounds-=1
                if self.controllerToNotify.playingSounds>1{
                    self.controllerToNotify.mixMusicLabel.text = String(self.controllerToNotify.playingSounds) + " contents"
                } else if self.controllerToNotify.playingSounds==1{
                    self.controllerToNotify.mixMusicLabel.text = "1 content"
                }
                else{
                    UIView.animate(withDuration: 0.5, delay: 0, options: [],
                                                               animations: {
                                                                   self.controllerToNotify.mixMusicView.alpha=0
                               }, completion: nil)
                               self.controllerToNotify.playPauseButton.isSelected = false

                }
            }
        }
    
    func setAnchorPoint(_ point: CGPoint) {
           var newPoint = CGPoint(x: bounds.size.width * point.x, y: bounds.size.height * point.y)
           var oldPoint = CGPoint(x: bounds.size.width * layer.anchorPoint.x, y: bounds.size.height * layer.anchorPoint.y);

           newPoint = newPoint.applying(transform)
           oldPoint = oldPoint.applying(transform)

           var position = layer.position

           position.x -= oldPoint.x
           position.x += newPoint.x

           position.y -= oldPoint.y
           position.y += newPoint.y

           layer.position = position
           layer.anchorPoint = point
       }
    
    func animateView (){
        rotation.fromValue = -Double.pi/8
        rotation.toValue = Double.pi/8
        rotation.duration = 1.5 // or however long you want ...
        rotation.autoreverses = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.setAnchorPoint (CGPoint(x: 0.5, y:0))
        self.isHighlighted = true
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    func stopAnimateView(){
        self.isHighlighted = false
       self.setAnchorPoint (CGPoint(x: 0.5, y:0.5))
       self.layer.removeAllAnimations()
    }
}


class UIAnimalSoundView : UISoundImageView {
    override var category: String {
        get {
            return "animal"
        }
        set {
            super.category = "animal"
        }
    }
    
    
    
}

class UINatureSoundView : UISoundImageView {
    override var category: String {
        get {
            return "nature"
        }
        set {
            super.category = "nature"
        }
    }
}

class UIWaterSoundView : UISoundImageView {
    override var category: String {
        get {
            return "water"
        }
        set {
            super.category = "water"
        }
    }
}

