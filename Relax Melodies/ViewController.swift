//
//  ViewController.swift
//  Relax Melodies
//
//  Created by Alessandro Palermo on 05/12/2019.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var soundsButton: UIButton!
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var allFilterButton: UIButton!
    @IBOutlet weak var natureFilterButton: UIButton!
    @IBOutlet weak var waterFilterButton: UIButton!
    @IBOutlet weak var animalFilterButton: UIButton!
    
    @IBOutlet weak var catView: UIAnimalSoundView!
    @IBOutlet weak var moonView: UINatureSoundView!
    @IBOutlet weak var frogView: UIAnimalSoundView!
    @IBOutlet weak var birdView: UIAnimalSoundView!
    @IBOutlet weak var rainView: UIWaterSoundView!
    @IBOutlet weak var waterfallView: UIWaterSoundView!
    @IBOutlet weak var campfireView: UINatureSoundView!
    @IBOutlet weak var riverView: UIWaterSoundView!
    @IBOutlet weak var windView: UINatureSoundView!
    
    @IBOutlet weak var halfLine: UIImageView!
    @IBOutlet weak var line3: UIImageView!
    @IBOutlet weak var line2: UIImageView!
    @IBOutlet weak var line1: UIImageView!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var volumeView: UIView!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var volumeSlider: UISlider!
    @IBOutlet weak var mixMusicView: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var mixMusicLabel: UILabel!
    
    var sound : Sound!
    var listOfSounds : [UISoundImageView]!
    var filterButtons : [UIButton]!
    
    var playingSounds : Int = 0
    var catPosition : CGPoint!
    var moonPosition : CGPoint!
    var frogPosition : CGPoint!
    var birdsPosition : CGPoint!
    var rainPosition : CGPoint!
    var waterfallPosition : CGPoint!
    var campfirePosition : CGPoint!
    var riverPosition : CGPoint!
    var windPosition : CGPoint!

    
    override func viewDidLoad() {
        view.backgroundColor = UIColor(patternImage: UIImage(named: "wallpaper1.jpg")!)
        soundsButton.isSelected = true
        enableFilterButton(button: allFilterButton)

        filterButtons = [allFilterButton, natureFilterButton, waterFilterButton, animalFilterButton]
        listOfSounds = [catView, moonView, frogView, birdView, rainView, waterfallView, campfireView, riverView, windView]
        
        listOfSounds.forEach{view in
            view.controllerToNotify = self
        }
        setInitialPosition()
        super.viewDidLoad()
    }
    

    @IBAction func SoundsButtonTapped(_ sender: Any) {
        soundsButton.isSelected = true
        musicButton.isSelected = false
        view.backgroundColor = UIColor(patternImage: UIImage(named: "wallpaper1.jpg")!)
        self.scrollView.alpha = 1
    }
    
    @IBAction func MusicButtonTapped(_ sender: Any) {
        musicButton.isSelected = true
        soundsButton.isSelected = false
        self.view.backgroundColor = UIColor.white
        self.scrollView.alpha = 0

    }
    
    @IBAction func AllFilterButtonTapped(_ sender: Any) {

        disableFilterButtons()
        enableFilterButton(button: allFilterButton)
        self.scrollView.isScrollEnabled = true

         catView.frame.origin = catPosition
         moonView.frame.origin = moonPosition
         frogView.frame.origin = frogPosition
         birdView.frame.origin = birdsPosition
         rainView.frame.origin = rainPosition
         waterfallView.frame.origin = waterfallPosition
         campfireView.frame.origin = campfirePosition
         riverView.frame.origin = riverPosition
         windView.frame.origin = windPosition
    }
    
    @IBAction func natureFilterButtonTapped(_ sender: Any) {
        disableFilterButtons()
        self.enableFilterButton(button: self.natureFilterButton)
        self.listOfSounds.forEach{view in
            if view.category != "nature"{
                view.alpha = 0
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {

            self.campfireView.frame.origin.x = self.catView.frame.origin.x
            self.campfireView.frame.origin.y = self.catView.frame.origin.y
            self.windView.frame.origin.x = self.frogView.frame.origin.x
            self.windView.frame.origin.y = self.frogView.frame.origin.y
            self.halfLine.frame.origin.x = self.line1.frame.origin.x
            self.line1.alpha = 0
            self.line2.alpha = 0
            self.line3.alpha = 0
        }
    }
    @IBAction func waterFilterButtonTapped(_ sender: Any) {

        disableFilterButtons()
        self.enableFilterButton(button: self.waterFilterButton)
        self.listOfSounds.forEach{view in
             if view.category != "water"{
                 view.alpha = 0
             }
         }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {

            self.rainView.frame.origin.x = self.moonView.frame.origin.x
            self.rainView.frame.origin.y = self.moonView.frame.origin.y
            self.waterfallView.frame.origin.x = self.frogView.frame.origin.x
            self.waterfallView.frame.origin.y = self.frogView.frame.origin.y
            self.riverView.frame.origin.x = self.catView.frame.origin.x
            self.riverView.frame.origin.y = self.catView.frame.origin.y
            self.halfLine.frame.origin.x = self.line1.frame.origin.x
            self.line1.alpha = 0
            self.line2.alpha = 0
            self.line3.alpha = 0
        }
    }
    @IBAction func animalFilterButtonTapped(_ sender: Any) {
        self.disableFilterButtons()
        self.enableFilterButton(button: self.animalFilterButton)
        self.listOfSounds.forEach{view in
             if view.category != "animal"{
                 view.alpha = 0
             }
         }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {

            
            self.birdView.frame.origin.x = self.moonView.frame.origin.x
            self.birdView.frame.origin.y = self.moonView.frame.origin.y
            self.halfLine.frame.origin.x = self.line1.frame.origin.x
            self.line1.alpha = 0
            self.line2.alpha = 0
            self.line3.alpha = 0
        }
    }
    
    func disableFilterButtons(){
        filterButtons.forEach{button in
            if button.isSelected{
                button.isSelected = false
                       button.layer.backgroundColor = UIColor.clear.cgColor
                       button.setTitleColor(UIColor.white, for: .normal)
            }
        }
        
        listOfSounds.forEach{view in
                 view.alpha = 1
         }
        resetViewsPosition()
    }
    
    func enableFilterButton(button: UIButton){
        button.isSelected = true
        button.layer.backgroundColor = UIColor.white.cgColor
        button.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func playPauseButton(_ sender: Any) {
        if playPauseButton.isSelected {
           pauseMixMusic()
        } else {
            playMixMusic()
        }
    }
    
    func playMixMusic(){
        playPauseButton.isSelected = true
        listOfSounds.forEach{view in
            if view.sound.isPaused {
                view.sound.resumeSound()
                view.animateView()
            }
        }
    }
    
    func pauseMixMusic(){
        playPauseButton.isSelected = false
        listOfSounds.forEach{view in
           if view.sound.isPlaying {
               view.sound.pauseSound()
                view.stopAnimateView()
           }
       }
    }
    
    func setInitialPosition(){
        catPosition = catView.frame.origin
        moonPosition = moonView.frame.origin
        frogPosition = frogView.frame.origin
        birdsPosition = birdView.frame.origin
        rainPosition = rainView.frame.origin
        waterfallPosition = waterfallView.frame.origin
        campfirePosition = campfireView.frame.origin
        riverPosition = riverView.frame.origin
        windPosition = windView.frame.origin

    }
    
    func resetViewsPosition(){
        self.scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y), animated: false)
        self.scrollView.isScrollEnabled = false

        line1.alpha = 1
        line2.alpha = 1
        line3.alpha = 1
    }
    
    @IBAction func volumeSliderChanged(_ sender: Any) {
         listOfSounds.forEach{view in
            if view.restorationIdentifier == volumeLabel.text {
                view.sound.volume = volumeSlider.value/100
                view.sound.player?.volume = view.sound.volume
            }
        }
    }
}

