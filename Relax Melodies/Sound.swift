//
//  Sound.swift
//  Relax Melodies
//
//  Created by Alessandro Palermo on 05/12/2019.
//

import Foundation
import UIKit
import AVFoundation

class Sound {
    var filename : String!
    var category : SoundCategory!
    var isPlaying : Bool = false
    var isPaused : Bool = false
    var volume: Float = 0.5
    
    init(name: String) {
        self.filename = name
    }
    
    enum SoundCategory {
        case animal
        case water
        case nature
    }
    
    

    var player: AVAudioPlayer?

    func playSound(name: String) {
        filename = name
        guard let url = Bundle.main.url(forResource: filename, withExtension: "mp3") else {
            print("No such file")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)
            try AVAudioSession.sharedInstance().setActive(true)

            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            
            player.volume = self.volume
            player.numberOfLoops = -1
            player.play()
            self.isPlaying = true
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func stopSound() {
        player?.stop()
        self.isPlaying = false
        self.isPaused = false
    }
    
    func pauseSound(){
        player?.pause()
        self.isPaused = true
    }
    
    func resumeSound(){
        player?.play()
        self.isPaused = false
    }

}

